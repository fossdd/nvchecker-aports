#!/bin/bash

# ntfy.sh server the notification will be sent to
NTFY_SERVER="${NTFY_SERVER:-https://ntfy.sh}"
NTFY_TOPIC="${NTFY_TOPIC:-nvchecker}"

# Name or email address used in the Maintainer line in your maintained aports
MAINTAINER_NAME="${FULL_NAME:-fossdd}"

# Username of your account on gitlab.alpinelinux.org
GITLAB_USERNAME="${GITLAB_USERNAME:-fossdd}"

# Password or access token of your account on gitlab.alpinelinux.org
GITLAB_TOKEN="${GITLAB_TOKEN}"

# Space seperated list of teams you'll additionally will be notified
TEAMS="${TEAM:-phosh gnome}"

# If SSH should be used for git operations
USE_SSH="${USE_SSH:-false}"

msg() { echo ">>> $@"; }
[[ -d /usr/share/abuild ]] && source /usr/share/abuild/functions.sh && enable_colors

dir=$(realpath $(dirname $))
cd $dir

cd_aports() {
	cd $(mktemp -d)
	msg "Cloning aports ($1) into $PWD"
	git clone $1 $PWD --depth 1
}

aports_url_of() {
	if $USE_SSH; then
		echo git@gitlab.alpinelinux.org:$1/aports.git;
	else
		echo https://$GITLAB_USERNAME:$GITLAB_TOKEN@gitlab.alpinelinux.org/$1/aports.git;
	fi
}

cd_alpine_aports() {
	cd_aports $(aports_url_of alpine)
}

cd_own_aports() {
	cd_aports $(aports_url_of $GITLAB_USERNAME)
}

cmd_diff() {
	maintaining=$(mktemp)
	configs=$(mktemp)

	cd_alpine_aports

	msg "Collecting maintained packages..."
	for name in $MAINTAINER_NAME $TEAMS; do
		rg "maintainer=.*$name.*" \
			| cut -f1 -d":" \
			| sed 's/\/APKBUILD//g' \
			| tee -a $maintaining > /dev/null
	done

	# legacy "# Maintainer: "
	for name in $MAINTAINER_NAME $TEAMS; do
		rg "Maintainer: .*$name" \
			| cut -f1 -d":" \
			| sed 's/\/APKBUILD//g' \
			| tee -a $maintaining > /dev/null
	done

	cat $maintaining | sort -u | tee $maintaining-1 > /dev/null
	mv $maintaining-1 $maintaining

	msg "Collecting configured packages..."
	cd $dir
	find configs -type f \
		| sort \
		| sed 's/configs\///g' \
		| sed 's/\.toml//g' \
		| tee $configs > /dev/null
	cat $configs | sort -u | tee $configs > /dev/null

	msg "Difference between maintained and configured packages:"
	diff -U3 --color=always $configs $maintaining
}

add_default_header() {
	f=$(mktemp).toml
	pkg=${1##*/}
	data=$dir/.data/${pkg/\.toml/}

	touch $f
	mkdir -p $data

	cat <<EOF > $f
[__config__]
oldver = "$data/old_ver.json"
newver = "$data/new_ver.json"

EOF

	cat $1 >> $f

	echo $f
}

cmd_check() {
	cd $dir/configs
	find -type f | sort | while read f; do
		msg "Checking '$f'..."
		nvchecker -c $(add_default_header $f) $@;
	done
}

cmd_compare() {
	cd $dir/configs
	find -type f | sort | while read f; do
		msg "Comparing '$f'..."
		nvcmp -c $(add_default_header $f) $@;
	done
}

cmd_take() {
	cd $dir/configs
	find -type f | sort | rg $1 | while read f; do
		msg "Taking '$f'..."
		nvtake -c $(add_default_header $f) $@;
	done
}

cmd_take_all() {
	cd $dir/configs
	find -type f | sort | while read f; do
		msg "Taking '$f'..."
		nvtake --all -c $(add_default_header $f) $@;
	done
}

cmd_tree() {
	cd $dir
	tree configs
	msg "In sum: $(find configs -type f | wc -l) aports"
}

ntfy() {
	msg "Sending notification via ntfy.sh..."
	curl -d "$@" $NTFY_SERVER/$NTFY_TOPIC
}

cat_unmaintained() {
	# TODO: support maintainer variable
	rg -U 'Maintainer:\n' \
		| sed 's/\/APKBUILD.*//' \
		| sort \
		| while read line;
	do
		printf "**$line:** ";
		grep '^pkgdesc=' $line/APKBUILD | sed s/pkgdesc=//g;
	done
}

cmd_unmaintained() {
	cd_alpine_aports
	cat_unmaintained
}

cmd_readme() {
	msg "Writing README.md file..."
	cd $dir
	rm -fr README.md
	cat <<EOF > README.md
# nvchecker-aports

Just my local tool to check new versions of packages.

Feel free to take a look :)

## Upgrades

EOF
	cmd_check 2>&1 >/dev/null | sort | while read line; do
		full="${line##*] }"
		sum="${full/taskName=Task-2/}"
		msg "Adding '$sum'..."
		cat <<EOF >> README.md
- [ ] $sum
EOF
		ntfy "$sum"
	done

	cat <<EOF >> README.md

EOF
}

cmd_auto() {
	msg "Pulling latest nvchecker-aports commits"
	git pull
	cmd_readme
	msg "Commiting changes..."
	git add .
	git commit -m 'Automatic data update by CI' -m "[ci skip]" --no-gpg-sign
	msg "Pushing commit to upstream..."
	git push
}


case $1 in
	# Runs the CI pipeline including git commits and writing README.md
	auto) cmd_auto;;
	# Checks for new releases and reports them to stdout
	check) cmd_check ${@:2};;
	# Runs nvcmp to compare old and new releases (needs ./tool check before)
	compare) cmd_compare ${@:2};;
	# Checks the consistence of config files and latest maintained aports
	diff) cmd_diff;;
	# Updates version records of all aports
	take_all) cmd_take_all ${@:2};;
	# Updates version record of the first argument as aport
	take) cmd_take ${@:2};;
	# Shows your maintained aports
	tree) cmd_tree;;
	# Writes the README.md
	readme) cmd_readme;;
	# Lists all unmaintained aports
	unmaintained) cmd_unmaintained;;
	*) echo "Unknown option. $0 [auto/check/compare/diff/take_all/take/readme]"; exit 1;;
esac
msg "Finished."
